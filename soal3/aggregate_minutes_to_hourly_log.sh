#!/bin/bash

minimum=(9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807)
maximum=(-9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808)
hour_file="metrics_"`date +%Y%m%d%H -d "1 hour ago"`

files=`ls ~/log | awk -v hour="$hour_file" '$0 ~ hour {print $0}'`
total=(0 0 0 0 0 0 0 0 0 0 0)
count_files=0
for file in $files
do
  ((count_files++))
  result=`awk -F "," 'NR == 2 {print}' ~/log/$file`
  IFS="," read -a spec <<< $result
  for i in ${!spec[@]}
  do
    if [[ $i == 9 ]]
    then
      continue
    fi
    if [[ ${spec[$i]} < ${minimum[$i]} ]]
    then
      if [[ $i == 10 ]]
      then
        minimum[$i]=${spec[$i]%?}
      else
        minimum[$i]=${spec[$i]}
      fi
    fi
    if [[ ${spec[$i]} > ${maximum[$i]} ]]
    then
      if [[ $i == 10 ]]
      then
        maximum[$i]=${spec[$i]%?}
      else
        maximum[$i]=${spec[$i]}
      fi
    fi
    if [[ $i == 10 ]]
    then
      total[$i]=$(( ${total[$i]} + ${spec[$i]%?} ))
    else
      total[$i]=$(( ${total[$i]} + ${spec[$i]} ))
    fi
  done
done

average=()

for i in ${!total[@]}
do
  if [[ $i == 9 ]]
  then
    average[$i]=`echo ~`
    continue
  fi
  cal=`bc -l <<<  ${total[$i]}/$count_files`
  avg=`printf "%g" $cal`
  average[$i]=$avg
done

maximum[9]=`echo ~`
minimum[9]=`echo ~`

minimum[10]+="M"
maximum[10]+="M"
average[10]+="M"

file_name="metrics_agg_"`date +%Y%m%d%H -d "1 hour ago"`".log"

touch ~/log/$file_name

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path_path_size" > ~/log/$file_name
echo "minimum,"$(IFS=",";echo "${minimum[*]}") >> ~/log/$file_name
echo "maximum,"$(IFS=",";echo "${maximum[*]}") >> ~/log/$file_name
echo "average,"$(IFS=",";echo "${average[*]}") >> ~/log/$file_name

chmod 400 ~/log/$file_name
