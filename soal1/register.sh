#!/bin/bash

printf "Username : "
read username

printf "Password : "
read -s password

echo " "

dateIn=`date +%D`
time=`date +%T`

passLenght=${#password}


if [[ ! -d users ]]
then
	mkdir users
fi

if grep -q -w $username users/user.txt
then
	echo "Username already used, please use another Username"
	
	echo $dateIn $time REGISTER:ERROR User already exist >> users/log.txt

elif [[ $password == $username ]]
then
	echo "Password can not be the same as Username"

elif [[ passLenght -lt 8 ]]
then
	echo "Password must at least consist of 8 character or more"

elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
then 
	echo "Password must have at least one uppercase, lowercase, and number"
	
else
	echo "User registered successfully"
	echo $dateIn $time REGISTER:INFO User $username registered successfully >> users/log.txt
	echo $username $password >> users/user.txt
	
fi
