# soal-shift-sisop-modul-1-A12-2022

Member
- Christhoper Marcelino Mamahit (5025201249)
- I Putu Bagus Adhi Pradana (5025201010)
- Yusron Nugroho Aji (5025201138)

## Soal 1

Pada soal nomor 1 kami diminta untuk membuat 2 buah script yaitu:
- `register.sh` sebagai sarana User untuk melahkukan registrasi akun, dimana data akun seperti `USERNAME` dan `PASSWORD` akan disimpan pada `./users/user.txt`.
- `main.sh`. sebagai sarana User untuk login ke akun yang sudah diregistrasi pada script `register.sh`.

Untuk `PASSWORD` sendiri saat melahkukan penginputan akan dihidden untuk menjaga keamanan, hal ini dilakukan pada kedua script.

### register.sh

Terdapat beberapa syarat dalam melahkukan registrasi pada `register.sh` yaitu:

- Kriteria untuk `PASSWORD`: 
  - Minimal 8 karakter.
  - Memiliki minimal 1 huruf kapital dan huruf kecil.
  - Alphanumeric.
  - Tidak boleh sama dengan `USERNAME`.
- Saat User melahkukan registrasi akan dilakukan pencatatan pada `log.txt` dengan format `MM/DD/YY hh:mm:ss MESSAGE`. `MESSAGE` sendiri akan tergantung pada aksi berikut:
  - Ketika mencoba register dengan username yang sudah terdaftar, maka
message pada log adalah `REGISTER: ERROR User already exists`.
  - Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`.

Pada script `register.sh` akan meminta User untuk memasukan `USERNAME` dan `PASSWORD` yang ingin diregistrasi oleh User. Untuk meminta data tersebut digunakanlah potongan program berikut.

```bash
printf "Username : "
read username

printf "Password : "
read -s password

echo " "
```
Pada program diatas digunakan `-s` untuk menyembunyikan `PASSWORD` saat dilakukan input.

Setelah itu kami melahkukan beberapa pendeklarasian beberapa variable yang nantinya akan digunakan.

```bash
dateIn=`date +%D`
time=`date +%T`

passLenght=${#password}
```
Keterangan variable 
- `dateIn` digunakan untuk menyimpan tanggal dengan format `MM/DD/YY`. 
- `time` digunakan untuk menyimpan waktu dengan format `hh:mm:ss`.
- `passLenght` digunakan untuk menyimpan panjang dari `PASSWORD` yang diregistrasi oleh User.

Setelah itu dilakukan pengecekan apakah directory `users` sudah ada atau tidak. Jika directory `users` belum ada maka akan dilakukan pembuatan directory `users`.

```bash
if [[ ! -d users ]]
then
	mkdir users
fi
```
Setelah directory `users` sudah ada maka barulah dilakukan pengecekan apakah data `PASSWORD` yang dimasukan oleh User sudah memenuhi syarat syarat di atas.

Pertama yang dilakukan adalah pengecekan pada `USERNAME`. Dimana jika User memasukan `USERNAME` yang sudah terregistrasi maka registrasi akan gagal. Nantinya akan dicetak peringatan dan pencatatan pesan pada `log.txt` sesuai dengan format yang tertera pada syarat registrasi diatas.

```bash
if grep -q -w $username users/user.txt
then
	echo "Username already used, please use another Username"
	
	echo $dateIn $time REGISTER: ERROR User already exist >> users/log.txt
```  

Setelah dipastikan bahwa `USERNAME` yang dimasukan User belum terdaftar maka berikutnya akan dilakukan pengecekan terhadap `PASSWORD`.

- Jika `PASSWORD` saat dicek sama dengan `USERNAME` maka akan dilakukan pencetakan peringatan dan registrasi gagal.

  ```bash
  elif [[ $password == $username ]]
  then
	  echo "Password can not be the same as Username"
  ```

- Jika `PASSWORD` saat dicek memiliki kurang dari 8 karakter maka akan dilakukan pencetakan peringatan dan registrasi gagal.

  ```bash
  elif [[ passLenght -lt 8 ]]
  then
	  echo "Password must at least consist of 8 character or more"
  ```

- Jika `PASSWORD` saat dicek tidak mengandung setidaknya 1 huruf kapital, 1 huruf kecil, dan bukan alphanumeric maka akan dilahkukan pencetakan peringatan dan registrasi gagal.
  
  ```bash
  elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
  then 
	  echo "Password must have at least one uppercase, lowercase, and number"
  ```

Jika semua syarat sudah terpenuhi untuk melahkukan registrasi, maka akan dilakukan beberapa hal yaitu:
- Pencetakan pemberitahuan bahwa registrasi berhasil dilahkukan
- Pencatatan pesan ke `log.txt` sesuai dengan format yang tertera pada syarat diatas.
- Penyimpanan data yaitu `USERNAME` dan `PASSWORD` pada `user.txt`

```bash
else
	echo "User registered successfully"
	echo $dateIn $time REGISTER:INFO User $username registered successfully >> users/log.txt
	echo $username $password >> users/user.txt
	
fi
```

### main.sh

Sama seperti `register.sh`, `main.sh` juga memiliki beberapa syarat yaitu:

- Saat User melahkukan login akan dilahkukan pencatatan pada `log.txt` dengan format `MM/DD/YY hh:mm:ss MESSAGE`. `MESSAGE` sendiri akan tergantung pada aksi berikut:
  - Ketika user mencoba login namun passwordnya salah, maka message pada
log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`.
  - Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`.
- Pada script `main.sh` terdapat 2 command yang dapat dilahkukan oleh user yaitu:
  - `dl N` (`N` = Jumlah gambar yang akan didownload) untuk mendownload gambar dari [https://loremflickr.com/320/240](https://loremflickr.com/320/240) dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama `YYYY-MM-DD_USERNAME`. Gambar-gambar yang didownload juga memiliki format nama `PIC_XX`, dimana `XX` adalah penomoran secara berurutan. Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan `PASSWORD` User tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan `PASSWORD` sesuai dengan user.
  - `att` untuk menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
user yang sedang login saat ini.

Pada seperti sebelumnya, untuk login dengan `main.sh` User akan diminta untuk memasukan `USERNAME` dan `PASSWORD`.

```bash
printf "Username : "
read username

printf "Password : "
read -s password

echo " "
```
Setelah itu dideklarasi beberapa variable yang nantinya akan digunakan.

```bash
folder=`date +%Y-%m-%d`_$username

dateIn=`date +%D`
time=`date +%T`
```

Keterangan variable:
- `dateIn` sama seperti diatas digunakan untuk menyimpan tanggal
- `time` sama seperti diatas digunakan untuk menyimpan waktu 
- `folder` digunakan untuk menyimpan format penamaan directory yang nantinya akan dibuat, format penamaan ini berupa `YYYY-MM-DD_USERNAME`.

Setelah itu barulah dilakukan pengecekan apakah `USERNAME` dan `PASSWORD` yang dimasukan User sudah terregistrasi atau belum.
 
Jika memang saat dicek pada `user.txt` ternyata terbukti tidak ada maka akan dicetak peringatan login gagal dan dilahkukan pencatatan pesan sesuai syarat login diatas pada `log.txt`. 

```bash
else
	echo "Failed login attemp"
			
	echo $dateIn $time LOGIN:ERROR Failed login attemp on user $username >> users/log.txt
			
fi
```

Jika memang saat dicek pada `user.txt` ternyata memang terbukti ada maka akan dicetak pemberitahuan login berhasil dan dilahkukan pencatatan pesan sesuai syarat login diatas `log.txt`. Juga, akan ditampilkan command list yang dapat dilakukan oleh User.

```bash
if grep -q -w "$username $password" users/user.txt
then
	echo "Successfully logged in"
	echo $dateIn $time LOGIN:INFO User $username logged in >> users/log.txt
			
	echo "Command list : "
	echo "dl  - download picture"
	echo "att - count log in attemp"
	echo " "
	echo "Note:"
	echo "If choosing dl, please insert the quantity of picture that want to be downloaded"
			
	printf "Enter command : "
	read comm N
``` 
Nantinya User juga akan diminta untuk memilih salah satu command yang ingin dijalankan oleh User. User bisa memilih `dl` untuk mendownload gambar dan `att` untuk menghitung upaya login dari User itu sendiri. Jika User memilih command `dl` wajib menyertakan jumlah gambar yang ingin didownload.

Jika User memilih command `dl` dan sudah memasukan jumlah gambar yang akan didownload, maka akan dilakukan pengecekan apakah file zip dengan nama yang sama sudah ada atau tidak, untuk format nama file sendiri sesuai dengan yang ada pada variable `folder`. 

- Jika belum ada maka akan dilakukan pembuatan directory dengan format penamaan sesuai dengan variable `folder`. Setelah dibuat, maka akan dilakukan pendownloadan gambar sesuai dengan jumlah yang diinginkan oleh User. gambar yang diunduh nantinya akan direname dengan format sesuai syarat diatas dan disimpan pada directory yang baru saja dibuat. Kemudian directory tersebut akan di zip dan diberikan `PASSWORD` sesuai dengan `PASSWORD` dari User yang sedang menggunakan script `main.sh` ini. Setelah di zip maka directory yang sudah di zip akan di hapus dan menyisakan file zip dari directory tersebut.

  ```bash
  if [[ $comm == "dl" ]] && [[ -n N ]]
  then
	  if [[ ! -f $folder.zip ]]
	  then
		  mkdir $folder

		  for (( i=1; i<=$N; i++ ))
		  do	
			  wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
						
		  done	
					
		  zip -P $password -r $folder.zip $folder/
		  rm -rf $folder
			
      . . .

  ``` 
- Jika saat dicek ternyata file zip sudah ada, maka file zip tersebut akan di unzip. Lalu file zip yang sudah di unzip akan dihapus. Barulah dilakukan pendownloadan gambar baru yang nantinya akan disimpan pada directory dari file zip yang sudah di unzip sebelumnya. Kemudian directory tersebut akan di zip dan diberikan `PASSWORD` sesuai dengan `PASSWORD` dari User yang sedang menggunakan script `main.sh` ini. Setelah di zip maka directory yang sudah di zip akan di hapus dan menyisakan file zip dari directory tersebut.

  ```bash
 
      . . .

      else
	      unzip -P $password $folder.zip
		  rm $folder.zip
					
		  for (( i=1; i<=$N; i++ ))
		  do	
			  wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
						
		  done	
					
		  zip -P $password -r $folder.zip $folder/
		  rm -rf $folder
					
	  fi	
  ```

Jika User memilih command `att` maka akan dilakukan penghitungan jumlah percobaab login yang dilakukan User baik itu berhasil maupun gagal. Pertama akan dilakukan penghitungan jumlah line yang mengandung `USERNAME` dari User pada `log.txt`. Karena `log.txt` tidak hanya mengandung pesan saat login saja melainkan mengandung pesan saat registrasi juga, maka akan dimanfaatkan salah satu syarat dari registrasi yaitu tidak meregistrasikan `USERNAME` yang sudah ada yang menyebabkan line register pada `log.txt` yang mengandung `USERNAME` dari User hanya ada 1 saja sehingga untuk mengetahui jumlah percobaan login yang sudah dilakukan adalah total line yang login mengandung `USERNAME` dari User dikurangi line register yang mengandung `USERNAME` dari User yaitu 1. Dari perhitungan tersebutlah didapatkan jumlah login dari User.

```bash
elif [[ $comm == "att" ]]
then
	count=$(grep -o -i $username users/log.txt | wc -l)
				
	echo `expr $count - 1`
```

Dan Jika user memilih command yang tidak terdaftar pada command list maka akan dicetak peringatan bahwa command yang dimasukan tidak ada pada command list.

```bash
else
		echo "Your command choice not in command list"
				
	fi
```

## Soal 2
Pada soal 2 ini kami diminta untuk membuat sebuah script yaitu, soal2_forensic_dapos.sh , dimana soal2_forensic_dapos.sh memiliki fungsi untuk dapat menjalankan tugas yang akan dikerjakan, yaitu untuk menemukan rata-rata serangan per jam, ip paling banyak yang mengakses server dan berapakali ip tersebut mengakses server, jumlah request yang menggunakan curl sebagai user agent, dan daftar ip yang mengakses pada pukul 2.

```bash
nano soal2_forensic_dapos
```

Menetapkan hasil file akan disimpan di folder_name="forensic_log_website_daffainfo_log" dan menyimpan hasil output file ke ratarata.txt dan result.txt

```bash
folder_name="forensic_log_website_daffainfo_log"

[ -d $folder_name ] && rm -r $folder_name

mkdir $folder_name
```

Untuk dapat menemukan rata-rata request, digunakan awk untuk dapat menentukan lokasi dari IP dengan start=$3, dilakukan pengecekan untuk menemukan ip maka akan dianggap sebagai request. setelah itu dilakukan print dengan menampilkan hasil rata-rata yang dapat dihasilkan dari hasil n, yaitu jumlah keseluruhan ip dibagi dengan ($3-start)). Hasil akan disimpan di directory log_website_daffainfo.log ke dalam file ratarata.txt

```bash
awk -F ":" '
    NR==2 {start=$3} {++n} 
    END {print "Rata-rata serangan adalah sebanyak " n/($3-start) " requests per jam"}
' ../log_website_daffainfo.log > $folder_name/ratarata.txt
```

Sedangkan, untuk mendapatkan ip yang paling banyak mengakses server, dilakukan awk dan menggunakan sort, uniq -c , dan sort -r, menggunakan substr($2, 2, length($2)-2 agar mendapatkan ip untuk output keluarnnya dan menambhakan jumlah berapa banyak ip tersebut mencoba mengakses server untuk disimpan ke dalam result.txt 

```bash
awk -F ":" '{print $1}' ../log_website_daffainfo.log | sort | uniq -c | sort -r | 
awk 'NR == 1 {print "IP yang paling banyak mengakses server adalah: " substr(substr($2, 2, length($2)-2) " sebanyak " $1 " requests"}' > $folder_name/result.txt
	}
```

Untuk menemukan kata curl akan dilakukan pencarian menggunakan awk '/curl/, yang digunakan untuk menemukan kata kunci curl dan ketika telah menemukannya akan disimpan kedalam curl. Selanjutnya akan diprint ,diresult.txt, berapa banyak curl yang telah mengakses dengan menampilkan hasil dari curl yang telah didapatkan

```bash
	awk '/curl/ {++curl} 
    END {
        print "Ada " curl " requests yang menggunakan curl sebagai user-agent"
    }
    ' ../log_website_daffainfo.log >> $folder_name/result.txt
```

Sedangkan mengertahui IP Address yang mengakses pada pukul 2 dengan menggunakan, $3 ~ /02/", yang digunakan untuk mendapatkan pukul jam di 02 ,selanjutnya menyimpannya ke dalam ipIn2 dan dilakukan looping untuk dapat menggunakannya dalam print setiap ip yang mengakses dipukul 2. Setelah itu disimpan ke dalam result.txt untuk dapat menampilkan hasil secara keseluruhan.

```bash
awk -F ":" '{
        if ($3 ~ /02/) {          
            ipIn2[$1]=1
			}
        } 
        END {
        	for (i in ipIn2){
				print i
        }
    }' ../log_website_daffainfo.log >> $folder_name/result.txt
```

## Soal 3
Pada soal ini, kami diminta membuat program monitoring resource komputer, meliputi ram dan size dari suatu directory. Terdapat dua file yang diminta yaitu metrics_{YmdHms}.log berupa program monitoring resources yang berjalan otomatis setiap menit dan metrics_agg_{YmdH}.log berupa program agregasi monitoring resources yang berjalan otomatis setiap jam berdasakan informasi file-file monitoring yang digenerate tiap menit.

Berikut akan dijelaskan lebih dahulu program monitoring tiap menit
```bash
title_result="mem_total,mem_used,mem_free,mem_share,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
```
Diinialisasikan dahulu sebuah variabel bernama `title_result` untuk menampung judul data resources program. Jenis-jenis resources yang dipantau bersifat tetap (11 aspek di atas saja) sehingga ditaruh ke dalam variabel dan mudah dipanggil ke depannya.
```bash
num_result=`free -m | awk 'BEGIN {OFS=","} {if (NR == 2) printf "%d,%d,%d,%d,%d,%d,",$2,$3,$4,$5,$6,$7; else if (NR == 3) printf "%d,%d,%d,",$2,$3,$4}'`
num_result="${num_result}"`du -sh ~ | awk 'BEGIN {OFS=","} {print $2,$1}'`
```
Program di atas berfungsi untuk mengekstrak sumber data resources. Berdasarkan instruksi soal, dipanggilah fungsi `free -m` yang memberikan informasi komputer, lalu dioperkan ke fungsi awk untuk dimanipulasi. Kami hanya membutuhkan nilai di baris ke 2 dan 3 yang berisikan data numerik resources komputer.
Namun, kami perlu memanipulasi kembali output awk sebelumnya karena data masih terpisah dengan spasi. Oleh karena itu, kami memanggil fungsi awk kembali serta `du -sh` untuk menggabungkannya dengan informasi disk yang belum dipanggil sebelumnya. Dengan memanfaatkan `OFS` kami mengubah delimiter menjadi koma(,) sesuai permintaan soal.
```bash
file_format="metrics_"`date +'%Y%m%d%H%M%S'`".log"
touch ~/log/$file_format
```
File penampung ekstrak data tadi diminta memiliki format khusus berupa tanggal. Kami menempatkannya pada sebuah variabel `file_format` lalu mengisisnya dengan string metrics diiikuti pemanggilan fungsi `date` dengan format yang custom sesuai permintaan soal, yaitu tahun bulan tanggal jam menit detik, serta diakhiri oleh ekstensi log. Kemudian, kami buat file tersebut dengan fungsi `touch`.
```bash
echo $title_result > ~/log/$file_format
echo $num_result >> ~/log/$file_format

# 400 -> read only for user
chmod 400 ~/log/$file_format
```
Setelah file sudah dibuat, kami memindahkan data ke dalam file tersebut. Diawali dengan variabel `title_result` berupa judul tiap kolom data menggunakan tanda > karena file masih kosong sehingga tidak masalah overwrite isinya. Lalu tidak lupa juga data numeriknya pada variabel `num_result` yang dipindah dengan tanda >> supaya data di-append bukan meng-overwrite.
Selanjutnya, kami juga mengubah perizinan file menggunakan fungsi `chmod`. Kami memberikan nilai 400 yang berarti read hanya untuk user (4=write, 0=no permission, digit pertama untuk konfigurasi user). Tidak lupa juga path yang kami pakai mengarah ke `/home/{user}`


Berikut akan dijelaskan alur program monitoring tiap jam
```bash
minimum=(9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807 9223372036854775807)
maximum=(-9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808 -9223372036854775808)
```
Kami meninisialisasi variabel minimum (berisi MAX_INT) dan maksimum (berisi MIN_INT) sejumlah 11 elemen (jumlah data pada file metrics).
```bash
hour_file="metrics_"`date +%Y%m%d%H -d "1 hour ago"`
files=`ls ~/log | awk -v hour="$hour_file" '$0 ~ hour {print $0}'
```
Kami harus menyeleksi file yang di-generate sesuai jamnya. Kami menggunakan `ls` untuk mendapatkan semua file yang ada, lalu masuk ke dalam fungsi awk di mana kami memfilter dengan `$hour_file`. Variabel ini menyimpan format nama file metrics yang akan diolah, yaitu 1 jam sebelum current hour lalu menyimpannya ke dalam variabel `files`. Pada filter awk, kami menambahkan `$0 ~` supaya format file yang di-compare harus exact atau sama persis.
```bash
total=(0 0 0 0 0 0 0 0 0 0 0)
count_files=0
```
Untuk menghitung average, kami menyiapkan array `total` yang akan menyimpan total semua data di tiap index-nya dan variabel `count_files` yang akan menampung jumlah file yang diolah.
```bash
for file in $files
do
  ((count_files++))
  result=`awk -F "," 'NR == 2 {print}' ~/log/$file`
  IFS="," read -a spec <<< $result
  for i in ${!spec[@]}
  do
    if [[ $i == 9 ]]
    then
      continue
    fi
    if [[ ${spec[$i]} < ${minimum[$i]} ]]
    then
      if [[ $i == 10 ]]
      then
        minimum[$i]=${spec[$i]%?}
      else
        minimum[$i]=${spec[$i]}
      fi
    fi
    if [[ ${spec[$i]} > ${maximum[$i]} ]]
    then
      if [[ $i == 10 ]]
      then
        maximum[$i]=${spec[$i]%?}
      else
        maximum[$i]=${spec[$i]}
      fi
    fi
    if [[ $i == 10 ]]
    then
      total[$i]=$(( ${total[$i]} + ${spec[$i]%?} ))
    else
      total[$i]=$(( ${total[$i]} + ${spec[$i]} ))
    fi
  done
done
```
Semua file tersebut kami loop dan ekstrak isinya menggunakan awk. Kami hanya menggunakan nilai di line ke-2 karena di sanalah data numeriknya berada. Di tiap iterasi file, variabel `count_files` akan di-increment. Selanjutnya, terdapat `IFS=","` yang berfungsi memisahkan data yang dipisahkan oleh koma(,) menjadi array yang selanjutnya kami simpan ke dalam variabel `spec`.
Array spec kami iterasi, kemudian isinya kami bandingkan dengan variabel minimum & maximum, jika lebih kecil akan masuk minimum dan sebaliknya. Kami juga memperhatikan kolom ke-9 yang berisi path berupa string sehingga kami skip karena tidak butuh diproses secara matematis.
Index ke-10 yang berupa path_size juga dioperasikan khusus dengan tanda `%?` untuk menghapus karakter huruf di belakang sehingga hanya akan diperoleh angka.
Di if else terakhir, kami menjumlahkan semua data pada array total.
```bash
average=()

for i in ${!total[@]}
do
  if [[ $i == 9 ]]
  then
    average[$i]=`echo ~`
    continue
  fi
  cal=`bc -l <<<  ${total[$i]}/$count_files`
  avg=`printf "%g" $cal`
  average[$i]=$avg
done
```
Setelah mendapatkan nilai minimum dan maksimum, kami masih perlu mencari nilai average sehingga diinisialisasikanlah array `average` kosong yang akan diisi nantinya.
Seperti pada umumnya, kami menjumlahkan nilai maksimum dan minimum, dibagi 2 untuk mencari rata-ratanya, lalu di-append ke array. Kolom 9 langsung `echo ~` karena tidak butuh dicari rata-ratanya.
```bash
file_name="metrics_agg_"`date +%Y%m%d%H`".log"

touch ~/log/$file_name
```
Setelah itu, kami membuat file-nya dengan format tertentu. Kami memanfaatkan fungsi date untuk men-custom tanggal.
```bash
maximum[9]=`echo ~`
minimum[9]=`echo ~`

minimum[10]+="M"
maximum[10]+="M"
average[10]+="M"
```
Index ke-9 pada array minimum dan maximum juga di-echo kan ~ untuk men-generate pathnya.
Di index ke-10 tiap array, diappend huruf M sebagai kompensasi penghilangannya pada program sebelumnya ketika mengkalkulasi rata-rata.
```bash
file_name="metrics_agg_"`date +%Y%m%d%H -d "1 hour ago"`".log"

touch ~/log/$file_name

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path_path_size" > ~/log/$file_name
echo "minimum,"$(IFS=",";echo "${minimum[*]}") >> ~/log/$file_name
echo "maximum,"$(IFS=",";echo "${maximum[*]}") >> ~/log/$file_name
echo "average,"$(IFS=",";echo "${average[*]}") >> ~/log/$file_name

chmod 400 ~/log/$file_name
```
Terakhir, kami `touch` untuk membuat file agregasinya sesuai format nama yang diminta pada soal.
Lalu, kami memindahkan data ekstrak ke file tersebut. Kami menggunakan tanda > dan >> sesuai penjelasan sebelumnya. Kami juga menggunakan `IFS echo` untuk mengembalikan delimiter koma(,) yang sebelumnya dihilangkan. Perizinan file juga diubah dengan `chmod` menjadi read only untuk user sesuai penjelasan sebelumnya.

Berikut adalah cronjob yang digunakan untuk menjalankan kedua file di atas secara otomatis, yaitu di tiap menit dan tiap jam.
```bash
* * * * * /bin/bash [path]/minute_log.sh
0 * * * * /bin/bash [path]/aggregate_minutes_to_hourly_log.sh
```














