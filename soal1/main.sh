#!/bin/bash

printf "Username : "
read username

printf "Password : "
read -s password

echo " "

folder=`date +%Y-%m-%d`_$username

dateIn=`date +%D`
time=`date +%T`

if grep -q -w "$username $password" users/user.txt
then
	echo "Successfully logged in"
	echo $dateIn $time LOGIN:INFO User $username logged in >> users/log.txt
			
	echo "Command list : "
	echo "dl  - download picture"
	echo "att - count log in attemp"
	echo " "
	echo "Note:"
	echo "If choosing dl, please insert the quantity of picture that want to be downloaded"
	echo " "		
	printf "Enter command : "
	read comm N
			
	if [[ $comm == "dl" ]] && [[ -n N ]]
	then		
		if [[ ! -f $folder.zip ]]
		then
			mkdir $folder

			for (( i=1; i<=$N; i++ ))
			do
				wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
						
			done	
					
			zip -P $password -r $folder.zip $folder/
			rm -rf $folder
			
		else
			unzip -P $password $folder.zip
			rm $folder.zip
					
			for (( i=1; i<=$N; i++ ))
			do	
				wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
						
			done	
					
			zip -P $password -r $folder.zip $folder/
			rm -rf $folder
					
		fi	
				
	elif [[ $comm == "att" ]]
	then
		count=$(grep -o -i $username users/log.txt | wc -l)
				
		echo `expr $count - 1`
			
	else
		echo "Your command choice not in command list"
				
	fi
			
else
	echo "Failed login attemp"
			
	echo $dateIn $time LOGIN:ERROR Failed login attemp on user $username >> users/log.txt
			
fi
