#!/bin/bash

title_result="mem_total,mem_used,mem_free,mem_share,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

num_result=`free -m | awk 'BEGIN {OFS=","} {if (NR == 2) printf "%d,%d,%d,%d,%d,%d,",$2,$3,$4,$5,$6,$7; else if (NR == 3) printf "%d,%d,%d,",$2,$3,$4}'`
num_result="${num_result}"`du -sh ~ | awk 'BEGIN {OFS=","} {print $2,$1}'`

file_format="metrics_"`date +'%Y%m%d%H%M%S'`".log"
touch ~/log/$file_format

echo $title_result > ~/log/$file_format
echo $num_result >> ~/log/$file_format

# 400 -> read only for user
chmod 400 ~/log/$file_format

