folder_name="forensic_log_website_daffainfo_log"

[ -d $folder_name ] && rm -r $folder_name

mkdir $folder_name

awk -F ":" '
    NR==2 {start=$3} {++n} 
    END {print "Rata-rata serangan adalah sebanyak " n/($3-start) " requests per jam"}
' ../log_website_daffainfo.log > $folder_name/ratarata.txt


awk -F ":" '{print $1}' ../log_website_daffainfo.log | sort | uniq -c | sort -r | 
awk 'NR == 1 {print "IP yang paling banyak mengakses server adalah: " substr($2, 2, length($2)-2) " sebanyak " $1 " requests"}' > $folder_name/result.txt

echo "" >> $folder_name/result.txt

awk '/curl/ {++curl} 
    END {
        print "Ada " curl " requests yang menggunakan curl sebagai user-agent"
    }
    ' ../log_website_daffainfo.log >> $folder_name/result.txt

echo "" >> $folder_name/result.txt

awk -F ":" '{
        if ($3 ~ /02/) {          
            ipIn2[$1]=1
			}
        } 
        END {
        	for (i in ipIn2){
				print i
        }
    }' ../log_website_daffainfo.log >> $folder_name/result.txt
